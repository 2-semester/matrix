#pragma once
namespace WorkingWithMatrix
{
	using comparer = int (*)(int*, int*, int);

	int** lowTriangleMatrix(int);
	void displayMatrixLow(int**, int);
	// int** convertToHighTriangleMatrix(int**, int);
	void displayMatrixHigh(int**, int);
	int** highTriangleMatrix(int);
	int** sumLowHigh(int**, int**, int);
	int** squareMatrix(int);
	void fillSquareMatrixRandom(int**, int);
	int** transpositionSquareMatrix(int**, int);
	void displaySquareMatrix(int**, int);
	int** symmetricMatrix(int);
	void fillSymmetricMatrixRandom(int**, int);
	void displaySymmetricMatrix(int**, int);
	int** sumSymmetricMatrix(int**, int**, int);
	int** rectangularMatrix(int, int);
	void displayRectangularMatrix(int**, int, int);
	int* convertTo(int**, int, int);
	int** convertTo(int*, int, int);
	void fillRectangularMatrix(int**, int, int);
	void swap(int*&, int*&);
	void swap(int&, int&);
	void sortMatrix(int**, int, int, comparer);
	int compareAscending(int*, int*, int);
}