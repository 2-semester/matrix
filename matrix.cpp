#include "matrix.h"
#include "arrays.h"
#include <iostream>

int** WorkingWithMatrix::lowTriangleMatrix(int size)
{
	int** matrix = new int* [size];

	for (int i = 0; i < size; i++)
	{
		matrix[i] = WorkingWithArrays::allocateMemoryInt(i + 1);
		WorkingWithArrays::randomValues(matrix[i], i + 1);
	}

	return matrix;
}

void WorkingWithMatrix::displayMatrixLow(int** matrix, int size)
{
	for (int i = 0; i < size; i++)
	{
		WorkingWithArrays::displayArrayInt(matrix[i], i + 1);

		for (int j = i + 1; j < size; j++)
		{
			std::cout.width(5);
			std::cout << 0;
		}

		std::cout << std::endl;
	}
}

// int** WorkingWithMatrix::convertToHighTriangleMatrix(int** matrix, int size)
// {
// 	int** copy = new int* [size];

// 	for (int i = 0; i < size; i++)
// 	{
// 		copy[i] = WorkingWithArrays::allocateMemoryInt(size - i);

// 		for (int j = 0; j < size - i; j++)
// 		{
// 			copy[i][j] = matrix[j + i][i];
// 		}
// 	}

// 	return copy;
// }

void WorkingWithMatrix::displayMatrixHigh(int** matrix, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < i; j++)
		{
			std::cout.width(5);
			std::cout << 0;
		}

		WorkingWithArrays::displayArrayInt(matrix[i], size - i);

		std::cout << std::endl;
	}
}

int** WorkingWithMatrix::highTriangleMatrix(int size)
{
	int** matrix = new int* [size];

	for (int i = 0; i < size; i++)
	{
		matrix[i] = WorkingWithArrays::allocateMemoryInt(size - i);
		WorkingWithArrays::randomValues(matrix[i], size - i);
	}

	return matrix;
}

int** WorkingWithMatrix::sumLowHigh(int** lowMatrix, int** highMatrix, int size)
{
	int** resultMatrix = new int* [size];

	for (int i = 0; i < size; i++)
	{
		resultMatrix[i] = new int [size];

		for (int j = 0; j < size; j++)
		{
			if (i == j)
			{	
				resultMatrix[i][j] = lowMatrix[i][j] + highMatrix[i][0];
			} 
			else if (j > i)
			{
				resultMatrix[i][j] = highMatrix[i][j - i];
			} 
			else {
				resultMatrix[i][j] = lowMatrix[i][j];
			}
		}
	}

	return resultMatrix;
}

int** WorkingWithMatrix::squareMatrix(int size)
{
	int** matrix = new int* [size];

	for (int i = 0; i < size; i++)
	{
		matrix[i] = WorkingWithArrays::allocateMemoryInt(size);
	}

	return matrix;
}

void WorkingWithMatrix::fillSquareMatrixRandom(int** matrix, int size)
{
	for (int i = 0; i < size; i++)
	{
		WorkingWithArrays::randomValues(matrix[i], size);
	}
}

int** WorkingWithMatrix::transpositionSquareMatrix(int** matrix, int size)
{
	int** resultMatrix = new int* [size];

	for (int i = 0; i < size; i++)
	{
		resultMatrix[i] = WorkingWithArrays::allocateMemoryInt(size);
		
		for (int j = 0; j < size; j++)
		{
			resultMatrix[i][j] = matrix[j][i];
		}
	}

	return resultMatrix;
}

void WorkingWithMatrix::displaySquareMatrix(int** matrix, int size)
{
	for (int i = 0; i < size; i++)
	{
		WorkingWithArrays::displayArrayInt(matrix[i], size);
		std::cout << std::endl;
	}
	
}


int** WorkingWithMatrix::symmetricMatrix(int size) 
{
	int** matrix = new int* [size];

	for (int i = 0; i < size; i++)
	{
		matrix[i] = WorkingWithArrays::allocateMemoryInt(i + 1);
	}

	return matrix;
}


void WorkingWithMatrix::fillSymmetricMatrixRandom(int** matrix, int size)
{
	for (int i = 0; i < size; i++)
	{
		WorkingWithArrays::randomValues(matrix[i], i + 1);
	}
}

void WorkingWithMatrix::displaySymmetricMatrix(int** matrix, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			std::cout.width(5);

			if (i == j)
			{
				std::cout << matrix[i][i];
			} 
			else if (j > i)
			{
				std::cout << matrix[j][i];
			} 
			else {
				std::cout << matrix[i][j];
			} 
		}

		std::cout << std::endl;
	}
}


int** WorkingWithMatrix::sumSymmetricMatrix(int** firstMatrix, int** secondMatrix, int size)
{
	int** resultMatrix = new int* [size];

	for (int i = 0; i < size; i++)
	{
		resultMatrix[i] = new int [i + 1];

		for (int j = 0; j < i + 1; j++)
		{
			resultMatrix[i][j] = firstMatrix[i][j] + secondMatrix[i][j];
		}
	}

	return resultMatrix;
}


int** WorkingWithMatrix::rectangularMatrix(int n, int m)
{
	int** matrix = new int* [n];

	for (int i = 0; i < n; i++)
	{
		matrix[i] = WorkingWithArrays::allocateMemoryInt(m);
	}

	return matrix;
}

int* WorkingWithMatrix::convertTo(int** matrix, int n, int m)
{
	int* array = new int [n * m];

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			array[m * i + j] = matrix[i][j];
		}
	}

	return array;
}

int** WorkingWithMatrix::convertTo(int* array, int n, int m)
{
	int** matrix = new int* [n];

	for (int i = 0; i < n; i++)
	{
		matrix[i] = new int [m];

		for (int j = 0; j < m; j++)
		{
			matrix[i][j] = array[m * i + j];
		}
	}

	return matrix;
}


void WorkingWithMatrix::displayRectangularMatrix(int** matrix, int n, int m)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			std::cout.width(5);
			std::cout << matrix[i][j];
		}

		std::cout << std::endl;
	}
}


void WorkingWithMatrix::fillRectangularMatrix(int** matrix, int n, int m)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			std::cout << "[" << i + 1 << ", " << j + 1 << "]" << ": ";
			std::cin >> matrix[i][j];
		}
	}
}

void WorkingWithMatrix::swap(int*& a, int*& b)
{
	int* temp = a;
	a = b;
	b = temp;
}

void WorkingWithMatrix::swap(int& a, int& b)
{
	int temp = a;
	a = b;
	b = temp;
}

void WorkingWithMatrix::sortMatrix(int** matrix, int n, int m, comparer comparer)
{
	for (int i = 0; i < n; i++)
    {
        for (int j = n - 1; j > i; j--)
        {
            if (comparer(matrix[j], matrix[j - 1], m) < 0)
            {
				WorkingWithMatrix::swap(matrix[j], matrix[j - 1]);
            }
        }
    }
}

int WorkingWithMatrix::compareAscending(int* a, int* b, int m)
{
    return WorkingWithArrays::sumArray(a, m) - WorkingWithArrays::sumArray(b, m);
}